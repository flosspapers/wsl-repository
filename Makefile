paper = wsl-repository.pdf

$(paper): %.pdf: %.md
	pandoc -s -f markdown -t latex -o $@ $<


clean:
	$(RM) $(paper)
