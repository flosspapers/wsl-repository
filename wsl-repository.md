---
title: An Open Access Repository for the _Workshop de Software Livre_ (WSL)
authors:
  - name: Antonio Terceiro
    affiliation: COLIVRE, LAPPIS/UNB
  - name: Filipe Saraiva
    affiliation: Universidade de São Paulo
abstract: |
  This paper describes the creation of an open access, free of charge
  repository for WSL. This repository is available at
  http://wsl.softwarelivre.org/ and contains all the papers from all the
  editions of WSL since its inception in the year of 2000.
---

## Introduction

_WSL_ is a academic workshop collocated with _FISL_ (Portuguese acronym for
International Free Software Forum), the largest Free Software conference in
Brazil. Its goal is to showcase free software-related work developed by
universities, research centers, companies and hackers. _WSL_ has been held
together with FISL since its very first edition in the year of 2000, what at
the time of writing means it already had 15 editions.

In part because traditionally _WSL_ hadn't had a fixed set of core organizers,
there was never a fixed location for its papers in digital format. The _FISL_
organization stopped producing a paperback version of its proceeding some years
ago. This led to a situation where a large amount of the papers from previous
_WSL_ editions were not easily accessible, requiring trips to local university
libraries to have access to them.

The last two decades seen the emergence of a social and politic movement whose
purpose is release for public access in the internet, free of charge, the
scientific literature and all the data utilized in the production of the paper.
Named Open Access (OA), this movement is composed by scientists, professors,
researchers, publishers, scientific societies, activists, and more. It is
possible to see the repercussion of their work on the increasing number of OA
journals, the creation of rules from some research foundations for their
scientists to publish papers in OA journals, the creation of optional OA
subscription to authors in mainstream scientific publishers, etc.

Free software and OA share a common objective but for different type of works.
This objective is to share the knowledge, for the first in the form of source
code, for the second in the form of scientific papers. This objective guided us
in the creation of _WSL_ repository. How _WSL_ is an academic conference on free
software, follow the requirements of OA turns the repository compatible with the
philosophical principles of both movements.

This paper describes the design and implementation of a digital repository for
_WSL_, and the work that was carried out in order to recover the digital
versions of all papers since the beginning of the workshop.

## Design of the repository

The repository was developed as a website containg different pages for each
_WSL_ edition and their respective papers. There is a search page also, to allow
the research in an unified database for all _WSL_ papers from all editions
together.

The website pages are generated from metadata files of each _WSL_ edition. This
metadata provide information of each paper, for example, title, authors,
abstract, and more. A script will read these metadata and will generate the HTML
files of the website, in a process commonly know as "static site generator".

The metadata is stored in the YAML format. An example of the metadata structure
utilized in the repository is presented bellow:

```yaml
papers:
  - title: "Paper title"
    abstract: "This paper …"
    file: "paper1.pdf"
    code: 1
    authors:
      - name: First Author
        institution: XXXX
      - name: Second Author
        institution: XXXX
  - title: "Another paper"
    abstract: "This paper …"
    file: "paper2.pdf"
    code: 2
    authors:
      - name: Only author
      - institution: YYYY
```

From this metadata and the papers PDF files, the repository will produce a
static HTML website. This brings a few advantages:

- the hosting requirements are extremely low. All that is needed is a web
  server capable of serving static content.
- the repository maintainers do not need to care about performance issues,
  since teh only code that is executed when a user visits the repository is the
  already highly-optimized web server code.

The code of repository is free software licensed under the GPLv3. Its contents
(i.e. the full set of papers) is licensed under the Creative Commons
Attribution-NoDerivs 3.0 Unported (CC BY-ND 3.0) license, with full,
unrestricted access (i.e. no pay wall).

This choice of licensing and access model was made by a few reasons:

* to democratize the access to the content published in WSL, which after all,
  is about Free Software. No having an Open Access knowledge repository would
  be a serious philosophical contradiction.
* to create a solution that can possibly be enhanced, generalized, and adopted
  by other publications looking into building its own independent open access
  repository.

-----------------------------------------------------------------------

* layout of the repository
 metadata files
* building the site

* static HTML = minimal hosting requirements
* rebuildable locally = people can download the entirething

![Initial page of the WSL repository](images/wsl-home.png)

![Proceedings form WSL 2014](images/wsl-proceedings.png)

![Searching for "kernel" on all WSL papers](images/wsl-search.png)


* paper listing
* search
* others?

## Recovering papers from all previous WSL editions

* obtaining the papers
  * some past chairs still had the PDFs
  * some didn't have the PDFs anymore, or never replied
* extractingn data from the papers into the metadata files
* dealing with scans (OCR tips etc)

## Conclusions and future improvements

* possibilities: using the full list of articles in reviews
* it's possible to adapt the same code for different publications
